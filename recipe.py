from qvision import BaseRecipe
import torch
import torch.nn as nn
import torch.optim as optim
from qvision.utils import PreprocessSample, PostProcessLoss, Logger, BaseCheckpointer
from qvision.core.hook_classes.visualize import Visualizer
from qvision.core.hook_classes.scheduler import Scheduler
from termcolor import colored
import torch.nn.functional as fn

from dataset import get_train_val_iterators
from model import get_model
from losses import scan_2stmulti_loss as loss


class Checkpointer(BaseCheckpointer):
    def init_metrics(self):
        print('checkpointer loaded')
        cls_tags = self.args.classes.split(',')
        seg_tags = self.args.segmentation_tags.split(",")
        self.best_loss_dict = {}
        self.best_iou_dict = {}
        self.best_auc_dict = {}
        for c in cls_tags:
            self.best_loss_dict[f'{c}_scan'] = float("inf")
            self.best_auc_dict[f'{c}'] = 0
        for c in seg_tags:
            self.best_loss_dict[f'{c}_seg'] = float("inf")
            self.best_iou_dict[f'{c}'] = 0

    def load_metrics(self):
        self.init_metrics()

    def metrics_improved(self, last_metrics):
        improved = False
        lossdict = last_metrics["loss"]
        aucdict = last_metrics["multi_head_auc_scan"]
        ioudict = last_metrics['multi_pixel_iou_seg']
        for c in self.best_loss_dict:
            if float(lossdict[c]) < self.best_loss_dict[c]:
                print(
                    colored(
                        f"{c} loss improved from " + str(self.best_loss_dict[c])[:5] + " to " + str(lossdict[c])[:5],
                        "cyan",
                    )
                )
                self.best_loss_dict[c] = float(lossdict[c])
                improved = True
        for c in self.best_auc_dict:
            if self.best_auc_dict[c] < aucdict[c]:
                print(
                    colored(
                        f"{c} auc improved from "
                        + str(self.best_auc_dict[c])[:5]
                        + " to "
                        + str(aucdict[c])[:5],
                        "cyan",
                    )
                )
                self.best_auc_dict[c] = aucdict[c]
                improved = True
        for c in self.best_iou_dict:
            if self.best_iou_dict[c] < ioudict[c]:
                print(
                    colored(
                        f"{c} iou improved from "
                        + str(self.best_iou_dict[c])[:5]
                        + " to "
                        + str(ioudict[c])[:5],
                        "cyan",
                    )
                )
                self.best_iou_dict[c] = ioudict[c]
                improved = True
        return improved

    def get_cp_name(self, metrics, epoch):
        auc_str = 'auc'
        aucdict = metrics['multi_head_auc_scan']
        for c in aucdict:
            auc_str += f'_{c}_{str(aucdict[c])[:5]}'

        iou_str = 'iou'
        ioudict = metrics['multi_pixel_iou_seg']
        for c in ioudict:
            iou_str += f'_{c}_{str(ioudict[c])[:5]}'

        return f"model_{epoch}_{auc_str}_{iou_str}"


class CustomPreprocessSample(PreprocessSample):
    def pre_process(self, last_sample, device, **kwargs):
        input = last_sample['input']
        target = {
            'pixel_target': {k: last_sample['pixel_target'][k].long()
                             for k in last_sample['pixel_target']},
            'scan_target': {k: last_sample['scan_target'][k][:, 0]
                            for k in last_sample['scan_target']}
        }

        return input, target

class CustomVisualizer(Visualizer):
    def on_epoch_end(self, mode, epoch, last_metrics, **kwargs):
        """
        last_metrics has the following keys :
            'loss', 'auc_scan', 'accuracy_scan', 'confusion_matrix_scan', 'average_precision_scan'
        """
        if not (self.args.local_rank <= 0):
            return

        for metric in last_metrics.keys():
            if "loss" in metric:
                loss_dict = last_metrics[metric]
                for loss in loss_dict.keys():
                    self.writer.add_scalar(f"{loss}_loss/{mode}", float(loss_dict[loss]), epoch)
            elif "confusion" in metric:
                class_accuracies = self.get_class_accuracies(last_metrics["confusion_matrix_scan"])
                for i, v in enumerate(class_accuracies):
                    self.writer.add_scalar(f"class_{i}_accuracy/{mode}", class_accuracies[i], epoch)
            elif "multi" in metric:
                if "auc" in metric:
                    auc_dict = last_metrics[metric]
                    for k in auc_dict:
                        self.writer.add_scalar(f"{k}_auc/{mode}", auc_dict[k], epoch)
                if "pixel" in metric:
                    iou_dict = last_metrics[metric]
                    for k in iou_dict:
                        self.writer.add_scalar(f"{k}_iou/{mode}", iou_dict[k], epoch)
            else:
                try:
                    self.writer.add_scalar(f"{metric}/{mode}", last_metrics[metric], epoch)
                except Exception as e:
                    print(e)
                    print(last_metrics[metric])

        # add lr
        if mode == "train":
            lr = self.optimizer.param_groups[0]["lr"]
            self.writer.add_scalar("lr", lr, epoch)

        self.writer.add_scalar(f"grad_norm/{mode}", self.total_norm, epoch)
        self.total_norm = 0


class CancerRecipe(BaseRecipe):
    def __init__(self, args):
        self.args = args
        # args.segmentation_tags = self.args.classes
        self.device = torch.device('cuda')
        super().__init__()

    def set_dataloaders(self):
        return get_train_val_iterators(self.args)

    def set_model(self):
        model = get_model(self.args)
        self.model = model.to(self.device)
        if self.args.pretrain_path != "":
            self.load_model_from_checkpoint()

        freeze_encoder = getattr(self.args, 'freeze_encoder', False)
        if freeze_encoder:
            print('--freezing encoder layers--')
            for param in self.model.model_module.encoder.parameters():
                param.requires_grad = False

        return self.model

    def load_model_from_checkpoint(self):
        if self.args.pretrain_path != "":
            print("Loading network from : ", self.args.pretrain_path)
            checkpoint = torch.load(self.args.pretrain_path)
            pretrained_dict = checkpoint["network"]
            self.model.load_state_dict(pretrained_dict)
            self.model = self.model.to(self.device)

    def set_criterion(self):
        return loss(self.args, self.device)

    def set_optimizer(self, param_groups):
        optimizer = None
        if self.args.optimizer == "sgd":
            optimizer = optim.SGD(
                [{"params": params, "lr": self.args.lr} for params in param_groups],
                lr=self.args.lr,
                momentum=self.args.momentum,
                nesterov=self.args.nesterov,
                weight_decay=self.args.weight_decay,
            )
        elif self.args.optimizer == "adam":
            optimizer = optim.Adam(
                [{"params": params, "lr": self.args.lr} for params in param_groups],
                lr=self.args.lr,
                weight_decay=self.args.weight_decay,
            )
        return optimizer

    def register_hook_classes(self):
        hook_classes = []
        # preprocesssample
        preprocess = CustomPreprocessSample()
        hook_classes.append(preprocess)
        # postprocessloss
        postprocessloss = PostProcessLoss()
        hook_classes.append(postprocessloss)
        # visualiser
        visualizer = CustomVisualizer(self.args, self)
        hook_classes.append(visualizer)
        # logger
        metric_names = ["loss", "multi_head_auc", "multi_pixel_iou"]
        agg_lambdas = {}

        def seg_lam(last_output, last_sample, i, tag):
            clsout, segout = last_output
            output = fn.softmax(segout[i], dim=1)[:, 1]
            target = last_sample['target']['pixel_target'][f'{tag}_mask'][:,0]
            return output.detach().cpu(), target.detach().cpu()

        def cls_lam(last_output, last_sample, i, tag):
            clsout, segout = last_output
            output = fn.softmax(clsout[i], dim=1)[:, 1]
            target = last_sample["target"]["scan_target"][f"{tag}_target"]
            target = (target == -100) * 0 + (target != -100) * target
            return output.detach().cpu(), target.detach().cpu()

        agg_lambdas["multi_pixel_iou"] = {"seg": seg_lam}
        agg_lambdas["multi_head_auc"] = {"scan": cls_lam}
        loss_keys = [f"{cls}_scan" for cls in self.args.classes.split(",")] + [f"{cls}_seg" for cls in self.args.classes.split(",")]


        logger = Logger(self.args, metric_names=metric_names,
                        format_lambdas=agg_lambdas, loss_keys=loss_keys)
        hook_classes.append(logger)
        # scheduler
        scheduler = Scheduler(args=self.args, optimizer=self.optimizer)
        hook_classes.append(scheduler)
        # checkpointer
        checkpointer = Checkpointer(self.args, self.optimizer, self.model)
        hook_classes.append(checkpointer)
        return hook_classes
