from utils.loss_utils import loss_dict
import torch


def lung_3st_loss(args, device):
    loss_keys = args.loss_keys.split(",")
    loss_fns = {k: loss_dict[k](args, device) for k in loss_keys}

    def criterion(output, target):
        scan_output, lung_output, patch_output, pixel_output, diff_output = output
        diff_target = torch.zeros(diff_output.size(0)).float().to(device)
        map_dict = {
            "scan": (scan_output, target["scan_target"]),
            "patch": (patch_output, target["patch_target"]),
            "lung": (lung_output, target["lung_target"]),
            "dice": (pixel_output, target["pixel_target"]),
            "pixel": (pixel_output, target["pixel_target"]),
            "diff": (diff_output, diff_target),
        }
        output_loss = {k: loss_fns[k](*map_dict[k]) for k in map_dict if k in loss_fns}
        return output_loss

    return criterion


def scan_1st_loss(args, device):
    loss_keys = args.loss_keys.split(",")
    loss_fns = {k: loss_dict[k](args, device) for k in loss_keys}

    def criterion(output, target):
        map_dict = {"scan": (output, target["scan_target"])}
        output_loss = {k: loss_fns[k](*map_dict[k]) for k in map_dict if k in loss_fns}
        return output_loss

    return criterion


def patch_2st_loss(args, device):
    loss_keys = args.loss_keys.split(",")
    loss_fns = {k: loss_dict[k](args, device) for k in loss_keys}

    def criterion(output, target):
        scan_output, patch_output = output
        map_dict = {
            "scan": (scan_output, target["scan_target"]),
            "patch": (patch_output, target["patch_target"]),
        }
        output_loss = {k: loss_fns[k](*map_dict[k]) for k in map_dict if k in loss_fns}
        return output_loss

    return criterion


def seg_multihead_loss(args, device):
    loss_keys = args.loss_keys.split(",")
    seg_tags = args.segmentation_tags.split(",")
    tag_pixel_wts = args.seg_pixel_wts.split(",")
    assert "seg" in loss_keys, "seg needs to be in loss keys when using segmentation multihead loss"
    seg_tags = args.segmentation_tags.split(",")
    loss_fns = [loss_dict["seg"](args, device, int(wt)) for wt in tag_pixel_wts]

    def criterion(output, target):
        loss_dict = {}
        for i, tag in enumerate(seg_tags):
            loss_dict[tag] = loss_fns[i](output[i], target[f"{tag}_target"])
        return loss_dict

    return criterion


def seg_cls_loss(args, device):
    loss_keys = args.loss_keys.split(",")
    assert "scan" in loss_keys, "scan not in loss_keys"
    assert "seg" in loss_keys, "seg not in loss keys"

    loss_fns = {k: loss_dict[k](args, device) for k in loss_keys}

    def criterion(output, target):
        pixel_output, scan_output = output
        map_dict = {
            "scan": (scan_output, target["scan_target"]),
            "seg": (pixel_output, target["pixel_target"]),
        }
        output_loss = {k: loss_fns[k](*map_dict[k]) for k in map_dict if k in loss_fns}
        return output_loss

    return criterion


def scan_2st_loss(args, device):
    tag_pixel_wt = args.pixel_wt
    loss_fns = {"seg": loss_dict["seg"](args, device, int(tag_pixel_wt)), "scan": loss_dict["scan"](args, device)}

    def criterion(output, target):
        scan_output, pixel_output = output
        map_dict = {"scan": (scan_output, target["scan_target"]), "seg": (pixel_output, target["pixel_target"])}
        output_loss = {k: loss_fns[k](*map_dict[k]) for k in map_dict if k in loss_fns}
        return output_loss

    return criterion


def scan_2stmulti_loss(args, device):
    tag_pixel_wt = [float(x) for x in args.pixel_wt.split(",")]
    tag_cls_wt = [float(x) for x in args.tag_wt.split(",")]
    classes = args.classes.split(",")
    cls_wtdict = list(zip(classes, tag_cls_wt, tag_pixel_wt))
    print(cls_wtdict)

    def criterion(output, target):
        output_loss = {}
        scan_output, pixel_output = output
        for i, item in enumerate(cls_wtdict):
            cls, clswt, segwt = tuple(item)
            clslossfn = loss_dict['ce'](args, device, clswt)
            clsloss = clslossfn(scan_output[i],
                                target['scan_target'][f'{cls}_target'])
            output_loss[f'{cls}_scan'] = clsloss
            seglossfn = loss_dict['dseg'](args, device, segwt)
            segloss = seglossfn(pixel_output[i],
                                target['pixel_target'][f'{cls}_mask'])
            output_loss[f'{cls}_seg'] = segloss
        return output_loss

    return criterion


def scan_2stmulti_comp_combi_loss(args, device, ignore_index=-100.0):
    tag_pixel_wt = [float(x) for x in args.pixel_wt.split(",")]
    tag_cls_wt = [float(x) for x in args.tag_wt.split(",")]
    classes = args.classes.split(",")
    cls_wtdict = list(zip(classes, tag_cls_wt, tag_pixel_wt))
    print(f"Class weight dict: {cls_wtdict}")
    combination_alpha = getattr(args, "combination_head_alpha", 1)
    competition_alpha = getattr(args, "competition_head_alpha", 1)
    combination_head = getattr(args, "combination_head", False)
    competition_head = getattr(args, "combination_head", False)

    def criterion(output, target):
        output_loss = {}
        scan_class_output = output["class_out"]
        binary_seg_out = output["binary_seg_out"]
        if combination_head:
            competition_out = output["competition_seg_out"]  # B X num_tags X 960 X 960
        if combination_head:
            combination_out = output["combination_seg_out"]  # B X 1 X 960 X 960
        combination_target = 0.0
        for i, item in enumerate(cls_wtdict):
            cls, clswt, segwt = tuple(item)
            # Class loss
            clslossfn = loss_dict["ce"](args, device, clswt)
            clsloss = clslossfn(scan_class_output[i], target["scan_target"][f"{cls}_target"])
            output_loss[f"{cls}_scan"] = clsloss
            # Default segloss
            seglossfn = loss_dict["seg_logit"](args, device, segwt)
            ori_target = target["pixel_target"][f"{cls}_mask"]
            mask = ori_target >= 0  # - 100 used as ignore index
            ori_target = ori_target.float()  # B X 1 X 960 X 960
            segloss = seglossfn(binary_seg_out[i], ori_target)
            segloss = segloss * mask
            segloss = segloss.mean([2, 3])

            output_loss[f"{cls}_seg"] = segloss
            # Competition head seg loss
            if competition_head:
                comp_segloss = seglossfn(competition_out[:, i].unsqueeze(dim=1), ori_target)
                comp_segloss = mask.float() * comp_segloss  # B X 1 X 960 X 960
                comp_segloss = comp_segloss.mean([2, 3]) * competition_alpha

                output_loss[f"{cls}_seg_comp"] = comp_segloss

            combination_target += ori_target

        # Combination head seg loss
        if competition_head:
            combination_target = torch.where(
                combination_out < 0, torch.full_like(combination_target, -100.0), torch.full_like(combination_target, 1.0)
            )
            mask = combination_target >= 0
            seglossfn = loss_dict["seg_logit"](args, device, 1)
            comb_segloss = seglossfn(combination_out, combination_target)
            comb_segloss = mask.float() * comb_segloss
            comb_segloss = comb_segloss.mean([2, 3]) * combination_alpha
            output_loss["seg_comb"] = comb_segloss
        return output_loss

    return criterion
