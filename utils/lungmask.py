import cv2
from skimage import morphology as mph
import glob
import os
import numpy as np
import torch

class LungMask():
    def __init__(self, thorax_dir):
        self.thorax_dir = thorax_dir
        self.mask_keys = ['lmask', 'rmask', 'dlmask', 'drmask']
        self._compute_thorax_indices()

    def _compute_thorax_indices(self):
        """create a list of available ids"""
        print('loading thorax mask ids')
        # allids = glob.glob(f'{self.thorax_dir}/lmask/*')
        # allids = [os.path.basename(x).replace('.png','') for x in allids]
        # self.thorax_ids = set(allids)
        self.thorax_ids = torch.load('/home/backup_users/manoj/Projects/current_project/qvision/maskglob.pt')

    @staticmethod
    def _rz(arr, sz=320):
        """resize and threshold"""
        arr = cv2.resize(arr, (sz, sz))
        arr = (arr > 125)*1
        return arr

    @staticmethod
    def _er_dil(arr):
        """erode and then dilate the binary array"""
        arr = mph.erosion(arr, mph.disk(4))
        arr = mph.dilation(arr, mph.disk(4))
        return arr

    def _mask_dict(self, idx):
        """read lung and diaphragm masks of the idx"""

        if idx not in self.thorax_ids:
            # raise RuntimeError('mask does not exist for the specified id')
            mask = np.zeros((320,320))
            mask_dict = {'lmask':mask, 'rmask':mask, 'dlmask':mask, 'drmask':mask}
            return mask_dict

        mask_dict = {}
        for k in self.mask_keys:
            try:
                im_path = f'{self.thorax_dir}/{k}/{idx}.png'
                im = self._rz(cv2.imread(im_path, 0))
                mask_dict[k] = im
            except Exception as e:
                print(f'error while reading {idx}-{k}')
                print(e)

        return mask_dict

    def get_lung_masks(self, idx):
        """generate lung masks from raw lung and diaphragm masks"""
        mask_dict = self._mask_dict(idx)
        lim = ((mask_dict['lmask'] - mask_dict['dlmask']) > 0)*1
        rim = ((mask_dict['rmask'] - mask_dict['drmask']) > 0)*1
        lim = self._er_dil(lim)
        rim = self._er_dil(rim)
        return lim*1, rim*1