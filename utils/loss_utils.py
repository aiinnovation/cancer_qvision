import torch
import torch.nn as nn
import torch.nn.functional as F
from functools import reduce


celoss = nn.CrossEntropyLoss
ce_logit_loss = nn.BCEWithLogitsLoss
loss_type_dict = {
    "ce": celoss,
}


def scan_loss(args, device):
    if args.tag_wt:
        tag_wts = args.tag_wt.split(",")
        tag_wts = [float(x) for x in tag_wts]
    else:
        tag_wts = [1] * args.num_classes
    loss = loss_type_dict[args.loss_type]
    loss_scan = loss(torch.FloatTensor(tag_wts), reduction="none").to(device)
    alpha = getattr(args, "scan_alpha", 1)

    def sc_loss(output, target):
        loss = loss_scan(output, target) * alpha
        return loss

    return sc_loss


def scan_ce_loss(args, device, wt):
    loss = celoss
    loss_scan = loss(torch.FloatTensor([1, wt]), reduction="none").to(device)
    alpha = getattr(args, "scan_alpha", 1)

    def sc_loss(output, target):
        loss = loss_scan(output, target) * alpha
        return loss

    return sc_loss


def patch_loss(args, device):
    patch_wts = args.patch_wts.split(",")
    patch_wts = [float(x) for x in patch_wts]
    loss = loss_type_dict[args.loss_type]
    patch_criterions = [loss(torch.FloatTensor([1.0, wt]), reduction="none").to(device) for wt in patch_wts]
    alpha = args.patch_alpha

    def ptc_loss(output, target):
        patch_loss = (
            reduce(
                lambda x, y: x + y,
                [crit(output[:, :, i], target[:, i]) for i, crit in enumerate(patch_criterions)],
            )
            / len(patch_criterions)
        )
        return (patch_loss * alpha).squeeze(1)

    return ptc_loss


def lung_loss(args, device):
    lung_wts = args.lung_wts.split(",")
    lung_wts = [float(x) for x in lung_wts]
    loss = loss_type_dict[args.loss_type]
    lung_criterions = [loss(torch.FloatTensor([1.0, wt]), reduction="none").to(device) for wt in lung_wts]
    alpha = args.lung_alpha

    def lg_loss(output, target):
        lung_loss = (
            reduce(
                lambda x, y: x + y,
                [crit(output[:, :, i], target[:, i]) for i, crit in enumerate(lung_criterions)],
            )
            / len(lung_criterions)
        )
        return (lung_loss * alpha).squeeze(1)

    return lg_loss

def pixel_loss(args, device):
    pixel_wt = float(args.pixel_wt)
    loss = loss_type_dict[args.loss_type]
    loss_pixel = loss(torch.FloatTensor([1.0, pixel_wt]), reduction="none").to(device)
    alpha = getattr(args, "pixel_alpha", 1)

    def px_loss(output, target):
        return (loss_pixel(output, target) * alpha).mean([1, 2, 3])

    return px_loss


def pixel_loss_dynamicalpha(args, device, wt=1):
    pixel_wt = float(args.pixel_wt)
    loss = loss_type_dict[args.loss_type]
    loss_pixel = loss(torch.FloatTensor([1.0, pixel_wt]), reduction="none").to(device)
    alpha = args.pixel_alpha

    def px_loss(output, target):
        bs = output.size()[0]
        output_reshaped = output.transpose(1, 2).reshape(-1, *output.shape[2:])
        target = target.view(-1, *target.size()[2:])
        celoss = loss_pixel(output_reshaped, target).mean([1, 2])
        pixel_sum = target.sum([1, 2])
        alpha_tensor = target[0].numel() / (pixel_sum + 1)
        alpha_tensor = (((alpha_tensor > 0) * alpha_tensor) ** 0.6) * (pixel_sum > 0) + (pixel_sum == 0) * 1 + (pixel_sum < 0) * 0.4
        final_loss = celoss * alpha_tensor * alpha
        final_loss = final_loss.view(bs, -1).mean(1)
        return final_loss

    return px_loss


def pixel_loss_dynamicalpha_scan(args, device, wt=1):
    pixel_wt = wt
    loss = loss_type_dict[args.loss_type]
    loss_pixel = loss(torch.FloatTensor([1.0, pixel_wt]), reduction="none").to(device)
    alpha = args.pixel_alpha

    def px_loss(output, target):
        bs = output.size()[0]
        # output_reshaped = output.transpose(1, 2)
        # output_reshaped = output_reshaped.reshape(-1, *output_reshaped.shape[1:])
        target = target.view(-1, *target.size()[2:])
        celoss = loss_pixel(output, target).mean([1, 2])
        pixel_sum = target.sum([1, 2])
        alpha_tensor = target[0].numel() / (pixel_sum + 1)
        alpha_tensor = (((alpha_tensor > 0) * alpha_tensor) ** 0.6) * (pixel_sum > 0) + (pixel_sum == 0) * 1 + (pixel_sum < 0) * 0.4
        final_loss = celoss * alpha_tensor * alpha
        final_loss = final_loss.view(bs, -1).mean(1)
        return final_loss

    return px_loss

def seg_dynalpha(args, device, wt=1):
    pixel_wt = wt
    loss = loss_type_dict[args.loss_type]
    loss_pixel = loss(torch.FloatTensor([1., pixel_wt]), reduction='none').to(device)
    alpha = args.pixel_alpha
    def px_loss(output, target):
        bs = output.size()[0]
        target = target.view(-1, *target.size()[2:])
        celoss = loss_pixel(output, target).mean([1, 2])
        pixel_sum = target.sum([1, 2])
        alpha_tensor = (pixel_sum > 0)*1 + (pixel_sum == 0)*0.2 + (pixel_sum < 0)*0.3
        final_loss = celoss * alpha_tensor * alpha
        final_loss = final_loss.view(bs, -1).mean(1)
        return final_loss
    return px_loss

def seg_ce_loss(args, device, wt=1):
    loss = celoss
    loss_pixel = loss(torch.FloatTensor([1.0, wt]), reduction="none").to(device)
    alpha = getattr(args, "pixel_alpha", 1)

    def px_loss(output, target):
        target = target[:, 0]
        return (loss_pixel(output, target) * alpha).mean([1, 2])

    return px_loss


def seg_ce_logit_loss(args, device, wt=1):
    loss = ce_logit_loss
    loss_pixel = loss(pos_weight=torch.FloatTensor([wt]), reduction="none").to(device)
    alpha = getattr(args, "pixel_alpha", 1)

    def px_loss(output, target):
        # target = target[:, 0]
        return loss_pixel(output, target) * alpha

    return px_loss


def diff_loss(args, device):
    alpha = args.diff_alpha
    loss_diff = nn.L1Loss().to(device)

    def df_loss(output, target):
        return loss_diff(output[:, 1], target) * alpha

    return df_loss


loss_dict = {
    "ce": scan_ce_loss,
    "scan": scan_loss,
    "patch": patch_loss,
    "lung": lung_loss,
    "pixel": pixel_loss_dynamicalpha,
    "diff": diff_loss,
    "seg": seg_ce_loss,
    "dseg": seg_dynalpha,
    "seg_logit": seg_ce_logit_loss,
}
