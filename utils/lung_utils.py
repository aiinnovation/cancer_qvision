from skimage import transform, morphology as mph
import cv2
import numpy as np
from qvision.utils.new_transforms import scale

def lung_span(bim):
    """ return span of the binary mask """
    flat = bim.max(1)
    top = np.argmax(flat > 0)
    bottom = bim.shape[0] - np.argmax(flat[::-1] > 0)
    flat = bim.max(0)
    left = np.argmax(flat > 0)
    right = bim.shape[1] - np.argmax(flat[::-1] > 0)
    return top, bottom, left, right

def lung_bbox(bim, oim):
    """ crop lung from the original fs image """
    bim = transform.resize(bim, oim.shape)
    bim = (bim == bim.max())*1
    t, b, l, r = lung_span(bim)
    oim_crop = oim[t:b, l:r]
    bim_crop = bim[t:b, l:r]
    return oim_crop, bim_crop

def smallsz_erode(bim, ksize, f=8):
    """avoid using erode on high resolution images
       by applying the filter on a downsized image and then
       rescaling it to original size.
       especially necessary as erode takes extreme amount of
       time when applied on large images
    """
    h,w = bim.shape
    bim = np.array(bim, dtype=np.uint8)
    bim_sm = cv2.resize(bim, (int(w/f), int(h/f)))
    bim_sm = mph.erosion(bim_sm, mph.disk(int(ksize/f)))
    bim = cv2.resize(bim_sm, (w,h))
    return bim

def dummy_mask_from_gt(lmask, rmask, lunggt, patchgt):
    """ create dummy segmentation mask from patch and lung gts, to
        be used in segmentation. aim is to have -100 where the gts
        are 1 and 0 when gts are 0
    """
    def dummy_mask_from_sidegt(mask, sidegt, patchgt):
        if sidegt == 0:
            return np.zeros(mask.shape)
        t, b, _, _ = lung_span(mask)
        pspan = int((b - t)/3)
        mask = (scale(mask) > 0)*1
        mask[t:t+pspan] = patchgt[0]*mask[t:t+pspan]
        mask[t+pspan:t+2*pspan] = patchgt[1]*mask[t+pspan:t+2*pspan]
        mask[b-pspan:b] = patchgt[2]*mask[b-pspan:b]
        mask[mask == 1] = -100
        return mask
    lmask = dummy_mask_from_sidegt(lmask, lunggt[0], patchgt[:3])
    rmask = dummy_mask_from_sidegt(rmask, lunggt[1], patchgt[3:])
    return lmask + rmask