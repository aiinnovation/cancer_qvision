import os
import configargparse
import pandas as pd
import hashlib
import numpy as np
import cv2
import torch
import glob
import torch.utils.data as data
from torch.utils.data import DataLoader
from torchvision.transforms import Compose
from image_transforms import transforms as tsfms
import qvision.utils.new_transforms as new_tsfms
import multiprocessing
from utils.lungmask import LungMask
from utils.lung_utils import dummy_mask_from_gt
from random import randint
import time

class tfm_wrapper():
    def __init__(self, tfm_func, default_tfm, pass_through_prob=0.2):
        self.tfm_func = tfm_func
        self.prob = pass_through_prob
        self.default_tfm = default_tfm

    def __call__(self, arr):
        return self._apply(arr)

    def _apply(self, arr):
        rint = randint(0,100)
        if rint > int(100*self.prob):
            out = self.tfm_func(arr)
            return out
        else:
            return self.default_tfm(arr)


def get_train_val_iterators(args):
    print('setting up dataloaders')
    nb_train_samples = getattr(args, 'nb_train_samples', 10000)
    nb_val_samples = getattr(args, 'nb_val_samples', 1000)
    batch_size = getattr(args, 'batch_size', 32)
    train_ds = Dataset2Stream(args, 'train')
    val_ds = Dataset2Stream(args, 'val')
    num_workers = getattr(args, 'num_workers', 5)

    def get_dataloader(mode):
        ds = mode == 'train' and train_ds or val_ds
        sampling_wts = ds.sample_wts
        nb_samples = mode == 'train' and nb_train_samples or nb_val_samples
        print('nb_samples in {}: {}'.format(mode, nb_samples))
        sampler = data.sampler.WeightedRandomSampler(sampling_wts, nb_samples,
                                                     replacement=True)
        nproc = num_workers#int(multiprocessing.cpu_count()/2)
        dataloader = DataLoader(ds, sampler=sampler,
                                batch_size=batch_size, num_workers=nproc)
        return dataloader

    tr_D = get_dataloader('train')

    val_D = get_dataloader('val')

    return {
        'train': tr_D,
        'val': val_D
    }

def get_test_iterators(args):
    print('setting up dataloaders')
    nb_test_samples = getattr(args, 'nb_test_samples', 10000)
    batch_size = getattr(args, 'batch_size', 32)
    test_ds = Dataset2Stream(args, 'full')
    sampling_wts = test_ds.sample_wts
    nproc = multiprocessing.cpu_count()
    sampler = data.sampler.WeightedRandomSampler(sampling_wts, nb_test_samples,
                                                 replacement=False)
    dataloader = DataLoader(test_ds, sampler=sampler,
                            batch_size=batch_size, num_workers=nproc)
    return dataloader


class Dataset2Stream(data.Dataset):
    def __init__(self, args, mode='train'):
        """
        Dataset3Stream
        :param args:
        :param mode: can be train, val, full - train and val will use cv param
        to decide what indices to use,
        full mode uses all the indices.
        """
        self.args = args
        self.mode = mode
        self.master_csv = getattr(args, 'master_csv',
                                  '/data_nas2/processed/ChestXRays/csvs/allchest_training_patches.csv')
        self.patches_path = getattr(args, 'high_res_dir',
                                    '/fast_data91/processed/ChestXRays/patches/training_224_1.1')
        self.masks_path = getattr(args, 'scan_mask_dir',
                                  '/fast_data94/ChestXRays/nodule_data/mask_patches')
        self.thorax_path = getattr(args, 'thorax_mask_dir', '')
        self.patch_presence_dict = getattr(args, 'patch_presence_dict', '')
        self.mask_size = int(getattr(args, 'mask_size', 224))

        self.sources = getattr(args, 'sources', 'medall,ca,max').split(',')
        self.im_size = int(getattr(args, 'im_size', 224))
        self.aug_level = getattr(args, 'bcgaug', 2)
        new_tsfms.auglevel = int(self.aug_level)
        self.nb_patches = getattr(args, 'nb_patches', 6)
        self.cv_param = getattr(args, 'cv_param', 4)
        self.classes = getattr(args, 'classes',
                               'opacity,consolidation,nodule,fibrosis').split(',')
        #         self.targets = getattr(args, 'targets', '0,0,1').split(',')
        #         self.targets = [int(x) for x in self.targets]

        #         self.nb_targets = len(set(self.targets))

        self.sampling = str(getattr(args, 'sampling', '0.5,0.5,0.5')).split(',')
        self.sampling = [float(x) for x in self.sampling]
        self.mask_sampling_wt = getattr(args, 'mask_sampling_wt', 1)
        # self.sampling = [x/sum(self.sampling) for x in self.sampling]

        self.multiclass = getattr(args, 'multiclass', 0)
        self.print_stats = getattr(args, 'print_stats', 1)
        self.equal_source_sampling = getattr(args, 'equal_source_sampling', 1)
        self.lm = LungMask(self.thorax_path)
        self._setup()

    def __getitem__(self, index):
        idx = self.indices[index]
        scan_target = self._get_target(idx)
        inp, mask = self._get_im_mask_from_id(idx)
        return {'image_id': idx,
                'input': inp,
                'scan_target': scan_target,
                'pixel_target': mask,
                'mode': self.mode
                }

    def __len__(self):
        return len(self.indices)

    def _setup_transforms(self):
        default_tfm = Compose([
                new_tsfms.scale,
                new_tsfms.resize_hard(int(self.im_size)),
                # tsfms.CenterCrop(self.im_size),
                # tsfms.RandomIntensityJitter(0.3, 0.3, 0.3),
                new_tsfms.clip,
                tsfms.ToTensor(),
            ])

        if self.mode == 'train':
            train_transform = Compose([
                new_tsfms.scale,
                # new_tsfms.resize_hard(int(self.im_size*8/7)),
                # tsfms.RandomCrop(self.im_size),
                new_tsfms.gamma,
                new_tsfms.brightness,
                new_tsfms.contrast,
                # tsfms.RandomIntensityJitter(0.8, 0.8, 0.8),
                new_tsfms.add_noise,
                new_tsfms.smooth,
                new_tsfms.clip,
                tsfms.ToTensor(),
            ])
            self.transform = tfm_wrapper(train_transform, default_tfm, 0.2)
        else:
            self.transform = tfm_wrapper(default_tfm, default_tfm, 0)

    def _setup(self):
        self._classwts()
        self._preprocess_and_set_metadata()
        self._setup_transforms()
        if self.print_stats:
            self.print_dataset_stats()

    def _classwts(self):
        self.classname_to_samplewt = dict(zip(self.classes, self.sampling))
        self.classname_to_samplewt['nota'] = 0.5#float(max(self.sampling))
        self.classname_to_samplewt['normal'] = 0.5#float(max(self.sampling))
        print(self.classname_to_samplewt)

    def _get_metadata(self):
        print('reading csv')
        cls = self.classes
        tgs = [x for x in cls if x != 'normal' and x != 'nota']
        scan_tags = tgs.copy()
        p_tgs = [['{}-{}'.format(x, i) for i in ['left', 'right', 0, 1, 2, 3, 4, 5]] for x in scan_tags]
        for x in p_tgs:
            tgs += x

        dfcols = pd.read_csv(self.master_csv, nrows=1).columns.tolist()
        name_col = dfcols[0]

        tgs = [name_col] + tgs + ['normal']
        tgs = [x for x in tgs if x in dfcols]

        df = pd.read_csv(self.master_csv, header=0, index_col=0, usecols=tgs)

        df_ids = dict((x, 1) for x in list(df.index))
        print('globbing image folder for files')
        # fl = glob.glob(os.path.join(self.patches_path, '*'))
        fl = torch.load('/home/backup_users/manoj/Projects/current_project/qvision/trglob.pt')
        file_ids = {os.path.basename(x).replace('.png', ''):x for x in fl}
        # file_ids = dict((x, 1) for x in file_ids)
        common_ids = [x for x in df_ids if x in file_ids]
        common_ids = [x for x in common_ids if x.split('.')[0] in self.sources]
        print('discarding ids that dont exist')
        discard_ids = set(list(df.index)) - set(common_ids)
        print(f'discarding {len(discard_ids)} ids as they dont exist in images path')
        df = df.drop(discard_ids)
        print(f'length of df {len(df)}')
        return df

    def _get_ids_with_masks(self):
        mask_dir = self.args.scan_mask_dir
        ims = []
        for c in self.classes:
            ims += glob.glob(f'{mask_dir}/{c}/*')
        idxs = {os.path.basename(x).replace('.png', '') for x in ims}
        return idxs

    def _get_target(self, idx):
        tar = {f'{cls}_target': torch.LongTensor([(cls in self.id_to_classes[idx]) * 1])
                                   for cls in self.classes}
        return tar

    def _preprocess_and_set_metadata(self):
        id_to_classes = {}
        id_to_weight = {}
        class_counter = dict([(x, 0) for x in self.classes])
        class_counter['nota'] = 0
        class_counter['normal'] = 0
        self.mask_ids = self._get_ids_with_masks()
        print('loading metadata dict')
        metadata = self._get_datapoints_metadata()
        metadata = metadata.to_dict()
        # torch.save(metadata, f'/fast_data94/ChestXRays/alldata/{self.mode}_md.pt')
        indices = list(metadata['normal'])
        classes = self.classes+['normal']
        print(self.classes)
        source_class_counter = dict((x, {}) for x in classes)
        source_class_counter['nota'] = {}
        # source_class_counter['normal'] = {}
        print(source_class_counter.keys())
        for x in source_class_counter:
            source_class_counter[x] = dict((y, 0) for y in self.sources)

        for idx in indices:
            clses_for_idx = []
            for cls in classes:
                if metadata[cls][idx] == 1:
                    clses_for_idx.append(cls)
            if len(clses_for_idx) == 0:
                clses_for_idx.append('nota')
                # clses_for_idx.append('normal')


            for cls in clses_for_idx:
                class_counter[cls] += 1
                source_class_counter[cls][idx.split('.')[0]] += 1

            id_to_classes[idx] = clses_for_idx
            id_to_weight[idx] = sum([self.classname_to_samplewt[x]
                                     for x in clses_for_idx])

        self.metadata = metadata
        self.indices = indices
        self.id_to_classes = id_to_classes
        self.id_to_wt = id_to_weight

        self.class_counter = class_counter
        weighted_class_counter = {}
        for idx in self.indices:
            #             print(idx)
            clss = id_to_classes[idx]
            idx_src = idx.split('.')[0]
            weighted_class_counter[idx] = 0
            for cls in clss:
                weighted_class_counter[idx] += source_class_counter[cls][idx_src] / class_counter[cls]
        if self.equal_source_sampling:
            print('Sampling sources and tags equally')
            self.sample_wts = [id_to_weight[idx] / (weighted_class_counter[idx] * min(
                [self.class_counter[x] for x in id_to_classes[idx]]))
                               for idx in self.indices]
        else:
            self.sample_wts = [id_to_weight[idx] / min([self.class_counter[x] for x in id_to_classes[idx]])
                               for idx in self.indices]

        print('oversampling images with masks')
        for i, idx in enumerate(self.indices):
            if idx in self.mask_ids:
                self.sample_wts[i] = self.mask_sampling_wt*self.sample_wts[i]

    def print_dataset_stats(self):
        print(self.mode)
        print('dataset size: {}'.format(len(self.indices)))
        print('\nclass counter:')
        print(self.class_counter)

    def to_onehot(self, targets):
        onehot = [0] * self.nb_targets
        for x in targets:
            onehot[x] = 1
        return onehot

    def _get_datapoints_metadata(self):
        """
        :return: metadata of the required samples as per mode
        """
        metadata = self._get_metadata()
        all_ids = list(metadata.index)
        discard_ids = []
        """
        metadata_dir = './dataset_metadata'
        if not os.path.exists(metadata_dir):
            os.makedirs(metadata_dir)
        discard_id_path = metadata_dir+'/discard_ids_{}.pt'.format(self.mode)

        if os.path.isfile(discard_id_path):
            print('loading discard ids from file')
            discard_ids = torch.load(discard_id_path)
            metadata.drop(discard_ids, inplace=True)
            return metadata
        """

        if self.mode == 'train':
            discard_ids = [x for x in all_ids
                           if self._get_sha1mod8(x) == self.cv_param]
        elif self.mode == 'val':
            discard_ids = [x for x in all_ids
                           if self._get_sha1mod8(x) != self.cv_param]
        elif self.mode == 'full':
            discard_ids = []

        metadata.drop(set(discard_ids), inplace=True)
        return metadata

    def _get_lung_patch_gt_from_id(self, idx, cls):
        #         print(cls)
        classes = self.id_to_classes[idx]
        #         targets = self.id_to_targets[idx]
        cls_to_patch_gt = {}
        cls_to_lung_gt = {}

        lung_gt_neg = [0] * 2
        patch_gt_neg = [0] * 6

        if cls not in classes:
            return lung_gt_neg, patch_gt_neg

        try:
            lung_gt = [self.metadata[cls + '-' + side][idx] for side in
                       ['left', 'right']]
            patch_gt = [self.metadata[cls + '-' + str(i)][idx] for i in
                        range(6)]

            lung_gt = [x * 1 for x in lung_gt]
            patch_gt = [x * 1 for x in patch_gt]

            cls_to_patch_gt[cls] = patch_gt
            cls_to_lung_gt[cls] = lung_gt

            lung_gt = [0] * 2
            patch_gt = [0] * 6

            for i in range(2):
                lung_gt[i] += lung_gt[i] == 0 \
                              and cls_to_lung_gt[cls][i] or 0
            for i in range(6):
                patch_gt[i] += patch_gt[i] == 0 \
                               and cls_to_patch_gt[cls][i] or 0

            return lung_gt, patch_gt
        except:
            return [-100]*2, [-100]*6

    def _get_thorax_mask(self, idx):
        try:
            lim, rim = self.lm.get_lung_masks(idx)
            lim = cv2.resize(np.array(lim, np.uint8), (self.im_size, self.im_size))
            rim = cv2.resize(np.array(rim, np.uint8), (self.im_size, self.im_size))
            return lim, rim
        except Exception as e:
            print('error in thorax masks', e)
            return np.zeros((self.im_size, self.im_size)), \
                   np.zeros((self.im_size, self.im_size))

    def _fix_lung_gt(self, lung_gt, patch_gt):
        def fix_side_gt(side_gt, sidepatch_gt):
            if side_gt < 1 and max(sidepatch_gt) < 1:
                return side_gt
            elif max(sidepatch_gt) < 1:
                return side_gt
            else:
                side_gt = max(sidepatch_gt)
                return side_gt

        lung_gt[0] = fix_side_gt(lung_gt[0], patch_gt[:3])
        lung_gt[1] = fix_side_gt(lung_gt[1], patch_gt[3:])
        return lung_gt, patch_gt

    def _create_dummy_mask(self, lim, rim, scan_gt, lung_gt, patch_gt):
        if scan_gt == 0:
            return np.zeros((self.im_size, self.im_size))
        elif (scan_gt ==1) and ((lim.max() == 0) or (rim.max() == 0)):
            return np.ones((self.im_size, self.im_size))*-100
        else:
            mask = dummy_mask_from_gt(lim, rim, lung_gt, patch_gt)
            mask[mask == -100] = 2
            mask = np.array(mask, np.uint8)
            return mask

    def _get_all_masks(self, idx):
        clsforid = self.id_to_classes[idx]
        mask_dict = {}
        for cls in self.classes:
            scan_gt = (cls in clsforid) * 1
            lung_gt, patch_gt = self._get_lung_patch_gt_from_id(idx, cls)
            lung_gt, patch_gt = self._fix_lung_gt(lung_gt, patch_gt)
            mask_path = os.path.join(self.masks_path, cls, idx + '.png')
            lmask, rmask = self._get_thorax_mask(idx)
            try:
                im_mask = cv2.imread(mask_path, 0)
                if (im_mask is None) or (im_mask.size == 0):
                    im_mask = self._create_dummy_mask(lmask, rmask, scan_gt, lung_gt, patch_gt)
                else:
                    im_mask = np.array((im_mask > 0) * 1, np.uint8)
            except Exception as e:
                print('error in lung masks', e)
                im_mask = np.zeros((self.im_size, self.im_size))

            mask_dict[f'{cls}_mask'] = im_mask

        return mask_dict

    def _get_im_mask_from_id(self, idx):
        scan_path = os.path.join(self.patches_path, idx + '.png')
        resize_index = 8 / 7
        try:
            im = cv2.imread(scan_path, 0)
            if im is None:
                print(idx)
                im = np.zeros((self.im_size, self.im_size))
            if len(im.shape) == 3:
                im = im[:, :, :2].mean(2)
        except Exception as e:
            print('error in images',e)
            im = np.zeros((self.im_size, self.im_size))

        mask_dict = self._get_all_masks(idx)
        ccdict = {k: ((mask_dict[k] == 1) * 1).max() for k in mask_dict}

        if self.mode == 'train':
            im = new_tsfms.resize_hard_int(int(self.im_size * resize_index))(im)
            mask_dict = {k: new_tsfms.resize_hard_int(int(self.im_size * resize_index))(mask_dict[k]) for k in
                         mask_dict}
            func = new_tsfms.seg_randomsizedcrop_int(frac_range=(0.85, 1), output_shape=self.im_size)
            mask_dict['input'] = im
            mask_dict = func(mask_dict)
            im = mask_dict.pop('input')
            # mask_dict = {k: new_tsfms.resize_hard_int(self.im_size)(mask_dict[k]) for k in mask_dict}
        else:
            im = new_tsfms.resize_hard(int(self.im_size))(im)
            im = new_tsfms.centercrop(self.im_size)(im)
            mask_dict = {k: new_tsfms.resize_hard_int(int(self.mask_size))(mask_dict[k]) for k in mask_dict}

        mask_dict = {k: np.array(mask_dict[k], np.float64) for k in mask_dict}
        for k in mask_dict:
            msk = mask_dict[k]
            msk[msk == 2] = -100
            if ccdict[k] == 0:
                msk[msk == 1] = -100
            mask_dict[k] = tsfms.ToTensor()(msk)

        im = self.transform(im)
        return im, mask_dict

    def _get_sha1mod8(self, s):
        """Return the sha1 hash of the string s modulo 5.
        """
        sha1mod8 = int(hashlib.sha1(s.encode()).hexdigest()[-1], 16) % 8
        return sha1mod8
