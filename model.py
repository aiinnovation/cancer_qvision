import segmentation_models_pytorch as smp
from segmentation_models_pytorch.base import SegmentationHead
import torch
import torch.nn as nn
import torch.nn.functional as fn
from qvision.models.CBAM import CBAM
#
class ClassHead(nn.Module):
    def __init__(self, in_channels=512, out_channels=2):
        super(ClassHead, self).__init__()
        self.lungconv = nn.Sequential(
            nn.Conv2d(in_channels, 256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 128, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True),
            nn.Conv2d(128, 64, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.AdaptiveAvgPool2d((1, 1))
        )
        self.fc = nn.Linear(64, out_channels)

    def forward(self, x):
        x = self.lungconv(x)
        x = x.view(*x.size()[:2])
        x = self.fc(x)
        return x


class MultiHead(nn.Module):
    def __init__(self, model_module, num_tags, im_size=960):
        super(MultiHead, self).__init__()
        self.num_tags = num_tags
        self.im_size = im_size
        self.model_module = model_module
        self.relu = nn.ReLU(inplace=True)
        print('initializing model')
        ch_size, dec_ch_size = self._get_channel_size(im_size)
        print('done')
        # self.ch_conv = nn.Conv2d(ch_size, 512, kernel_size=3, stride=1, padding=1)
        self.cls_heads = torch.nn.ModuleList([ClassHead(ch_size) for i in range(num_tags)])
        # self.cbam_heads = torch.nn.ModuleList([CBAM(16) for i in range(num_tags)])
        self.seg_heads = torch.nn.ModuleList([self._segmentation_head(dec_ch_size) for i in range(num_tags)])
        # self.upsamphead = nn.UpsamplingBilinear2d(scale_factor=16.0)

    def _get_channel_size(self, im_size):
        dummy_input = torch.randn(2, 1, im_size, im_size)
        with torch.no_grad():
            encout = self.model_module.encoder(dummy_input)
            decout = self.model_module.decoder(*encout)
        channels = [x.size(1) for x in encout]
        dec_channels = decout.size(1)
        return channels[-1], dec_channels

    def _segmentation_head(self, in_channels=128, out_channels=2):
        seghead = nn.Sequential(
            # CBAM(in_channels),
            nn.Conv2d(in_channels, out_channels, 1, stride=1),
            # nn.BatchNorm2d(32),
            # self.relu,
            # nn.Conv2d(32, out_channels, 3, stride=1),
            nn.UpsamplingBilinear2d(size=(self.im_size, self.im_size))
        )
        return seghead

    def forward(self, x):
        enc_feat = self.model_module.encoder(x)
        cls_output = [self.cls_heads[i](enc_feat[-1]) for i in range(len(self.cls_heads))]

        decoder_feat = self.model_module.decoder(*enc_feat)
        seg_output = [self.seg_heads[i](decoder_feat) for i in range(self.num_tags)]

        return cls_output, seg_output

def get_model(args):
    segdic = {'unet': smp.Unet, 'fpn': smp.FPN, 'linknet': smp.Linknet, 'pspnet': smp.PSPNet, 'unetplusplus':smp.UnetPlusPlus,
              'dv3':smp.DeepLabV3, 'dv3plus':smp.DeepLabV3Plus, 'manet':smp.MAnet}
    print('arch: {}'.format(args.arch))
    model_fn = segdic[args.arch]
    im_size = getattr(args, 'im_size', 960)
    print(im_size)
    model_module = model_fn(args.enc, in_channels=1, classes=args.num_classes, activation=None)
    num_tags = len(getattr(args, 'classes', 'opacity').split(','))
    model = MultiHead(model_module, num_tags, im_size)
    return model
