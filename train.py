import hydra
from omegaconf import DictConfig, OmegaConf
from clearml import Task
import logging
from recipe import CancerRecipe
import torch

logger = logging.getLogger(__name__)

@hydra.main(config_name="config")
def run(cfg: DictConfig):
    print('saving config')
    torch.save(cfg, '/home/users/manoj/Projects/current_project/cancer_qvision/args.pt')
    recipe = CancerRecipe(cfg)
    recipe.setup(cfg)
    # print(OmegaConf.to_yaml(cfg))
    task = Task.init(project_name=cfg.project_name, task_name=cfg.exp_name)
    logger.info(f"Training started")
    recipe.fit(epochs=cfg.epochs)
    logger.info(f"Training finished")

if __name__ == '__main__':
    run()
